def test_syslog_ng_confs(host):
    assert host.file("/etc/syslog-ng/conf.d/conserver.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/procserv.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/ioclog.conf").exists
    assert host.file("/etc/syslog-ng/conf.d/iocputlog.conf").exists
